package Fracciones is
  type Fraccion is private;

  function "+" (X, Y: Fraccion) return Fraccion;
  function "-" (X: Fraccion) return Fraccion;
  function "-" (X, Y: Fraccion) return Fraccion;
  function "*" (X, Y: Fraccion) return Fraccion;
  function "/" (X, Y: Fraccion) return Fraccion;
  function "=" (X, Y: Fraccion) return Boolean;
  function Mcd (Numerador : Integer; Denominador : Positive) return Integer;
  function Inversa (F: Fraccion) return Fraccion;


  -- Operaciones de entrada/salida con la consola
  procedure Leer (F: out Fraccion);
  procedure Escribir (F: Fraccion);

  -- Constructor de números fraccionarios a partir de números enteros
  function "/" (X, Y: Integer) return Fraccion;

  -- Operaciones para obtener las partes de una fracción
  function Numerador (F: Fraccion) return Integer;
  function Denominador(F:Fraccion) return Positive;
  function Reducir (F: Fraccion) return Fraccion;


private
  type Fraccion is record
    Num: Integer;
    Den: Positive;
  end record;
end Fracciones;
