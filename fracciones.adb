with Ada.Text_Io,Fracciones;
use Ada.Text_Io,Fracciones;

package body Fracciones is

--Función creadora de Fracciones
function "/" (X, Y: Integer) return Fraccion is
  Fraccion_Aux, Fraccion_Reducida: Fraccion;
  begin
    if Y < 0 then                               --Comprobamos si el Denominador es negativo,
      Fraccion_Aux.Num:= -X;                    --ya que en este caso cambiaremos el signo
      Fraccion_Aux.Den:= -Y;                    --del denominador al numerador. El denominador solo puede ser positivo.
      if X < 0 then
        Fraccion_Aux.Num:= -X;                  --Si ambos números son negativos, cambiamos ambos signos
        Fraccion_Aux.Den:= -Y;                  --y será una Fracción positiva
      end if;
    else
      Fraccion_Aux.Num:= X;
      Fraccion_Aux.Den:= Positive(Y);
    end if;
      Fraccion_Reducida:= Reducir(Fraccion_Aux); --Devolvemos la fracción ya reducida
    return Fraccion_Reducida;
  end "/";

--Función suma de Fracciones
function "+" (X, Y: Fraccion) return Fraccion is
  Suma_Fracciones, Fraccion_Aux_1, Fraccion_Aux_2: Fraccion;
  begin
    Fraccion_Aux_1:= Reducir(X);                  --Reducimos siempre las fracciones al inicio
    Fraccion_Aux_2:= Reducir(Y);
    Suma_Fracciones.Num:= ((Fraccion_Aux_1.Num * Integer(Fraccion_Aux_2.Den)) + (Fraccion_Aux_2.Num * Integer(Fraccion_Aux_1.Den)));
    Suma_Fracciones.Den:= (Fraccion_Aux_1.Den * Fraccion_Aux_2.Den);
    return Suma_Fracciones;
  end "+";

--Función cambio de signo de Fracción
function "-" (X: Fraccion) return Fraccion is
    Fraccion_Aux: Fraccion;
  begin
    Fraccion_Aux:= Reducir(X);
    Fraccion_Aux.Num:= -(X.Num);
    Fraccion_Aux.Den:=  X.Den;
    return Fraccion_Aux;
  end "-";

--Función Resta de Fracciones
function "-" (X, Y: Fraccion) return Fraccion is
  Resta_Fracciones, Fraccion_Aux_1, Fraccion_Aux_2: Fraccion;
  begin
    Fraccion_Aux_1:= Reducir(X);
    Fraccion_Aux_2:= Reducir(Y);
    Resta_Fracciones:= Fraccion_Aux_1+(-Fraccion_Aux_2);
    return Resta_Fracciones;
  end "-";

--Función Multiplicación de Fracciones
function "*" (X, Y: Fraccion) return Fraccion is
  Multiplicacion_Fracciones, Fraccion_Aux_1, Fraccion_Aux_2: Fraccion;
  begin
    Fraccion_Aux_1:= Reducir(X);
    Fraccion_Aux_2:= Reducir(Y);
    Multiplicacion_Fracciones.Num:= Fraccion_Aux_1.Num * Fraccion_Aux_2.Num;
    Multiplicacion_Fracciones.Den:= (Fraccion_Aux_1.Den * Fraccion_Aux_2.Den);
    return Multiplicacion_Fracciones;
  end "*";

--Función que Invierte una Fracción
function Inversa (F: Fraccion) return Fraccion is
  Fraccion_Reducida, Fraccion_Inversa: Fraccion;
  begin
      Fraccion_Reducida:= Reducir(F);
      if F.Num < 0 then                                         --Comprobamos que el Numerador sea negativo, en caso de que lo sea
        Fraccion_Inversa.Num:=Integer(-Fraccion_Reducida.Den);  --al invertir la Fracción controlamos el signo, ya que al enviarlo al
        Fraccion_Inversa.Den:=Positive(-Fraccion_Reducida.Num); --Denominador, este no puede ser negativo. Por lo que dejamos siempre el
      else                                                      --signo negativo en el Numerador
        Fraccion_Inversa.Num:=Integer(Fraccion_Reducida.Den);
        Fraccion_Inversa.Den:=Positive(Fraccion_Reducida.Num);
    end if;
      return Fraccion_Inversa;
  end Inversa;

--Función división de Fracciones
function "/" (X, Y: Fraccion) return Fraccion is
  Division_Fracciones, Fraccion_Aux_1, Fraccion_Aux_2: Fraccion;
  begin
    Fraccion_Aux_1:= Reducir(X);  --Reducimos la primera Fracción
    Fraccion_Aux_2:= Inversa(Y);  --Invertimos y reducimos la segunda Fracción
    Division_Fracciones:= Fraccion_Aux_1*Fraccion_Aux_2;
    return Division_Fracciones;
  end "/";

--Función que comprueba que dos Fracciones sea iguales, devolviendo True o False
function "=" (X, Y: Fraccion) return Boolean is
  Fraccion_Aux_1, Fraccion_Aux_2: Fraccion;
  begin
      Fraccion_Aux_1:= Reducir(X);
      Fraccion_Aux_2:= Reducir(Y);
      if Fraccion_Aux_1.Num = Fraccion_Aux_2.Num and Fraccion_Aux_1.Den = Fraccion_Aux_2.Den then
        return true;
      else
        return false;
      end if;
  end "=";

--Función que nos devuelve el Numerador de una Fracción
function Numerador (F: Fraccion) return Integer is
    Fraccion_Aux: Fraccion;
    Numero: Integer;
  begin
    Fraccion_Aux:= Reducir(F);
    Numero:= Fraccion_Aux.Num;
    return Numero;
  end Numerador;

--Función que nos devuelve el Denominador de una Fracción
function Denominador(F:Fraccion) return Positive is
  Fraccion_Aux: Fraccion;
  Positivo: Positive;
  begin
    Fraccion_Aux:= Reducir(F);
    Positivo:= Fraccion_Aux.Den;
    return Positivo;
  end Denominador;

--Función que nos calcula el Máximo Común Divisior de dos números, en este caso se le pasa el Numerador y Denominador de una Fracción
function Mcd(Numerador: Integer; Denominador: Positive) return Integer is
  Num: Integer := abs(Numerador);
  Den: Integer := abs(Integer(Denominador));
begin
    while Num /=Den loop
      if Num > Den then
        Num:= Num - Den;
      else
        Den:= Den - Num;
      end if;
    end loop;
    return Num;
end Mcd;


--Función que nos reduce una Fracción, llamando a
function Reducir(F: Fraccion) return Fraccion is
  Max_Com_Div: Integer;
  Fraccion_Reducida: Fraccion;
begin
  Max_Com_Div:= mcd(F.Num, F.Den);            --Obtenemos el MCD
  Fraccion_Reducida.Num := F.Num/Max_Com_Div;
  Fraccion_Reducida.Den := F.Den/Max_Com_Div;
  return Fraccion_Reducida;
end Reducir;


--Procedimiento para obtener Numerador y Denominador y crear una Fracción
procedure Leer (F: out Fraccion) is
  package Digitos_ES is new Ada.Text_Io.Integer_Io(Integer);
  Numerador, Denominador: Integer;
begin
  Put("Introduzca el Numerador de la Fracción : ");
  Digitos_ES.Get (Numerador);
  New_Line;
  Put("Introduzca el Denominador de la Fracción : ");
  Digitos_ES.Get (Denominador);
  F:= Numerador/Denominador;
end Leer;

--Procedimiento para imprimir una Fracción
procedure Escribir (F: Fraccion) is
  package Numerador_Salida is new Ada.Text_Io.Integer_Io(Integer);
  package Denominador_Salida is new Ada.Text_Io.Integer_Io(Positive);
begin
  Put("Numerador de la Fracción :");
  Numerador_Salida.Put (F.Num);
  New_Line;
  Put("Denominador de la Fracción :");
  Denominador_Salida.Put (F.Den);
  New_Line;
end Escribir;

end fracciones;
